#!/bin/sh -ex
 
# We operate on a (patched) source checkout
cd ../source
 
# Write out the makefiles etc..
autoreconf -fi
./configure || (cat config.log; exit -1)
 
# Fake a build
mkdir -p .libs
cp /usr/lib64/gssproxy/proxymech.so .libs
make test_proxymech
make interposetest
make -C tests
cp $(which gssproxy) .
 
# Do something awful
rm -f /usr/bin/valgrind
touch /usr/bin/valgrind
chmod +x /usr/bin/valgrind
 
# And run the tests
exec ./tests/runtests.py --valgrind-cmd=''
